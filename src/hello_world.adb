with Ada.Text_IO;
with GNATCOLL.JSON;

package body Hello_World is

   --------------
   -- Callback --
   --------------

   function Callback (Request : AWS.Status.Data) return AWS.Response.Data is
      Name : constant String :=
        AWS.Status.Parameter (D => Request, Name => "name");

      Result_Json : constant GNATCOLL.JSON.JSON_Value :=
        GNATCOLL.JSON.Create_Object;
   begin
      Ada.Text_IO.Put_Line ("Received Request: " & AWS.Status.URL (Request));

      if Name /= "" then
         Result_Json.Set_Field (Field_Name => "Hello", Field => Name);
      else
         Result_Json.Set_Field (Field_Name => "Hello", Field => "World");
      end if;

      return AWS.Response.Build
          (Content_Type => "text/json", Message_Body => Result_Json.Write);
   end Callback;

end Hello_World;
