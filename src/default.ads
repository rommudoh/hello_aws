with AWS.Response;
with AWS.Status;

package Default is
   function Callback (Request : AWS.Status.Data) return AWS.Response.Data;
end Default;
