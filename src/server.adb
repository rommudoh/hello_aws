with Ada.Text_IO;

with AWS.Config;
with AWS.Dispatchers.Callback;
with AWS.Server;
with AWS.Services.Dispatchers.URI;

with Default;
with Hello_World;

procedure Server is
   The_Web_Server : AWS.Server.HTTP;
   The_Dispatcher : AWS.Services.Dispatchers.URI.Handler;
begin
   Ada.Text_IO.Put_Line ("Starting on localhost:8080, press 'q' to exit.");

   AWS.Services.Dispatchers.URI.Register
     (Dispatcher => The_Dispatcher, URI => "/hello",
      Action     => Hello_World.Callback'Access);

   AWS.Services.Dispatchers.URI.Register_Default_Callback
     (Dispatcher => The_Dispatcher,
      Action     => AWS.Dispatchers.Callback.Create (Default.Callback'Access));

   AWS.Server.Start
     (Web_Server => The_Web_Server, Dispatcher => The_Dispatcher,
      Config     => AWS.Config.Get_Current);

   AWS.Server.Wait (Mode => AWS.Server.Q_Key_Pressed);

   AWS.Server.Shutdown (Web_Server => The_Web_Server);
end Server;
