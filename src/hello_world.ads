with AWS.Response;
with AWS.Status;

package Hello_World is
   function Callback (Request : AWS.Status.Data) return AWS.Response.Data;
end Hello_World;
